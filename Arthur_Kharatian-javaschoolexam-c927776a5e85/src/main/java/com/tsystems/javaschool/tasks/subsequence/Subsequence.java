package com.tsystems.javaschool.tasks.subsequence;

import java.util.Arrays;
import java.util.List;

public class Subsequence {
    public static void main(String[] args) {
        Subsequence s = new Subsequence();
        boolean b = s.find(Arrays.asList("A", "B", "C", "D"),
                Arrays.asList("BD", "A", "ABC", "B", "M", "D", "M", "C", "DC", "D"));
        System.out.println(b); // Result: true
    }

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        boolean flag = true;
        int n = 0;
        if (x != null && y != null) {

        for (int i = 0; i < x.size(); i++) {
            if (!flag) { break;}
            flag = false;
            for (int j = n; j < y.size(); j++) {
                if (y.get(j).equals(x.get(i))) {
                    n = j + 1;
                    flag = true;
                    break;
                }
            }
        }
    }else {throw new IllegalArgumentException();}
        return flag;
    }
}
