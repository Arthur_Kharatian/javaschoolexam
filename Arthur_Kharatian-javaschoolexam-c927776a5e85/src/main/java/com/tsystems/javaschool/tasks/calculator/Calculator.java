package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.Stack;

public class Calculator {

    public static void main(String[] args) {
        Calculator c = new Calculator();
        System.out.println(c.evaluate("(1+38)*4-5")); // Result: 151
        System.out.println(c.evaluate("7*6/2+8")); //  Result: 29
        System.out.println(c.evaluate("-12)1//(")); // Result: null
        System.out.println(c.evaluate("15/66")); // Result: 0.2273
        System.out.println(c.evaluate("15/4")); // Result: 3.75

    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        try {

            if (!checkStatement(statement)) return null;

            String result = operationsStack(statement);

            double digit = numStack(result);

            if (digit == Math.floor(digit)) {
                return Integer.toString((int) digit);
            } else {

                return new DecimalFormat("#.####").format(digit * 1.0).replaceFirst(",",".");
            }

        } catch (ArithmeticException e) {
            return null;
        }
    }

    private boolean checkStatement(String s) {
        if (s == null || s.isEmpty()) return false;
        char[] ch = s.toCharArray();
        boolean isOperation = true, isDot = true;
        int braces = 0;
        for (int i = 0; i < s.length(); i++) {
            if (!isOperation(ch[i]) && !isNumber(ch[i]) && !isDel(ch[i]) && ch[i] != ')' && ch[i] != '(') return false;
            if (ch[i] == ')') braces++;
            if (ch[i] == '(') braces--;
            if (isOperation(ch[i]) && !isOperation && (ch[i] != '(' && ch[i] != ')')) return false;
            if (isOperation(ch[i]) && (ch[i] != '(' && ch[i] != ')')) isOperation = false;
            if (!isOperation(ch[i]) || (ch[i] == '(' || ch[i] == ')')) isOperation = true;
            if (ch[i] == '.' && !isDot) return false;
            if (ch[i] == '.') isDot = false;
            if (ch[i] != '.') isDot = true;
        }
        return braces == 0;
    }

    private double numStack(String input) {
        char[] chars = input.toCharArray();
        Stack<Double> numStack = new Stack();
        for (int i = 0; i < chars.length; i++) {
            if (isDel(chars[i])) {
                continue;
            }
            if (isNumber(chars[i])) {
                String s = "";
                while (!isOperation(chars[i]) && !isDel(chars[i])) {
                    s += chars[i];
                    i++;
                    if (i == chars.length) break;
                }
                i--;
                numStack.push(Double.parseDouble(s));
            }
            if (isOperation(chars[i])) {
                numStack.push(doOperations(numStack.pop(), numStack.pop(), chars[i]));
            }
        }


        return numStack.pop();

    }

    private double doOperations(double b, double a, char c) {
        switch (c) {
            case '/': {
                if (b == 0) throw new ArithmeticException();
                return a / b;
            }
            case '*':
                return a * b;
            case '+':
                return a + b;
            case '-':
                return a - b;
            default:
                return -1;
        }
    }


    private String operationsStack(String input) {
        char[] chars = input.toCharArray();

        Stack<Character> operStack = new Stack();
        String result = "";
        for (int i = 0; i < input.length(); i++) {

            if (isDel(chars[i]))
                continue;

            if (isNumber(chars[i])) {
                while (!isDel(chars[i]) && !isOperation(chars[i])) {

                    result += chars[i];
                    i++;
                    if (i == input.length()) break;
                }
                result += " ";
                i--;
            } else if (chars[i] == '(') {
                operStack.push(chars[i]);

            } else if (chars[i] == ')') {
                char c = operStack.pop();
                while (c != '(') {
                    if (operStack.empty()) return null;
                    result += c;
                    c = operStack.pop();
                }
            } else if (isOperation(chars[i])) {
                if (operStack.empty()) operStack.push(chars[i]);
                else {

                    while (!operStack.empty() && (priority(chars[i]) <= priority(operStack.peek()))) {
                        result += operStack.pop();

                    }
                    operStack.push(chars[i]);

                }
            }


        }
        while (!operStack.empty()) {
            result += operStack.pop();
        }
        return result;
    }

    private boolean isOperation(char c) {
        return "+-/*()".indexOf(c) != -1;
    }


    private boolean isDel(char c) {
        return " ".indexOf(c) != -1;
    }


    private boolean isNumber(char c) {
        return "1234567890.".indexOf(c) != -1;
    }


    private int priority(char c) {
        switch (c) {
            case '/':
                return 3;
            case '(':
                return 0;
            case '+':
                return 2;
            case '-':
                return 2;
            case '*':
                return 3;
            case ')':
                return 1;
            default:
                throw new IllegalArgumentException();
        }

    }

}
