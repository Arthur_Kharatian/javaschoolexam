package com.tsystems.javaschool.tasks.pyramid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    public static void main(String[] args) throws IOException {
        PyramidBuilder pyramid = new PyramidBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter numbers ->");
        String line = reader.readLine();
        String[] digits = line.replaceAll("[^\\s0-9]", "").split(" ");

        List<Integer> numbers = new ArrayList<>();
        for (String digit : digits) {
            numbers.add(Integer.parseInt(digit));
        }
        pyramid.buildPyramid(numbers);

    }

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int height = 0;
        int listSize = inputNumbers.size();
        long l = 0;
        long temp;

        for (int i = 1; l < listSize; i++) {
            temp = i;
            l = (temp*temp+temp)/2;
            height = i;
        }
        int width = height*2-1;

        if (l != listSize) {
            throw new CannotBuildPyramidException();
        }

        int[][] pyramidArray = new int[height][width];
        if(inputNumbers.isEmpty()) {throw new CannotBuildPyramidException();}
            try {
                Collections.sort(inputNumbers);
            } catch (Exception e) {
                throw new CannotBuildPyramidException();
            }

        int getNumber = 0;
        boolean flag;

        for (int i = 0; i < height; i++) {
            flag = true;
            for (int j = 0; j < width; j++) {
                pyramidArray[i][j] = 0;
                if((i + j) >= (width - 1)/2 && (i + j) <= (width - 1)/2 + i * 2){
                 if(flag) {
                     pyramidArray[i][j] = inputNumbers.get(getNumber);
                     getNumber++;
                     flag = false;
                 }else {flag = true;}
                }
            }
        }
        for (int[] ints : pyramidArray) {
            System.out.println(Arrays.toString(ints));
        }

        return pyramidArray;
    }


}
